Description:
------------
Webform2vTiger module extends the Drupal Webform 3.x module with the ability
to capture webform results as a new lead in vTiger CRM.

The module allows you to map each field of Drupal Webform to a desired field
of vTiger new lead. But for new leads to be created in vtiger CRM you have to
map all required fields of vTiger leads.

Any Drupal Webform field can be mapped only to one field of vTiger lead.
All the mapped fields are shown in Drupal Webform form components page
(node/webform_node_id/webform).

To remove mapping for a field you have to edit that field and in
"Map field to vTiger CRM" choose "Do Not Map" and save.

Webform2vTiger module configuration page (admin/settings/webform2vtiger)
allows automatically configure required fields and get all custom fields
from vtiger CRM, if before saving you will check
"Try the url address of vtiger CRM specified above." After saving
configuration you can uncheck this and work on settings only locally. But
be adviced not to uncheck any fields that vtiger requires, or submissions
will not create new leads.

Please be careful to match correctly field types while mapping them.

After Webform submission you can see result code in Drupal recent log entries
(admin/reports/dblog).

This module is based in part on webform2sugar module, but extended and has its
own specifics due to vTiger using different variable names, paths,
application key, no campaigns for new leads, etc.
