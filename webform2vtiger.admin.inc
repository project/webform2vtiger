<?php

/**
 * @file
 * Administration pages provided by webform2vtiger module.
 */

/**
 * Menu callback: For admin/settings/webform2vtiger.
 *
 * Settings for accessing webform2vtiger.
 *
 * @return array
 *   Form array for admin settings menu.
 *
 * @see webform2vtiger_menu()
 * @see webform2vtiger_admin_settings_submit_callback()
 * @see webform2vtiger_vtigercrm_url_validate()
 */
function webform2vtiger_admin_settings() {
  $form['field_connection'] = array(
    '#type' => 'fieldset',
    '#title' => t('vtiger CRM connection settings'),
    '#description' => '<p>' . t('Allows you to configure your connection to your vtiger CRM. When providing your vtiger url, do not use a / at the end.') . '</p>',
  );

  $form['field_connection']['webform2vtiger_vtigercrm_url'] = array(
    '#type' => 'textfield',
    '#title' => t('The url address of vtiger CRM'),
    '#default_value' => variable_get('webform2vtiger_vtigercrm_url', ''),
    '#description' => t('The url address of where vtiger CRM can be found. No trailing "/". IE: http://example.com/crm'),
    '#weight' => 0,
    '#element_validate' => array('webform2vtiger_vtigercrm_url_validate'),
  );
  $form['field_connection']['webform2vtiger_try_url'] = array(
    '#type' => 'checkbox',
    '#title' => t('Try the url address of vtiger CRM specified above.'),
    '#default_value' => TRUE,
    '#description' => t('If checked, module will try to connect to your vtiger CRM using specified username and access key and will try to get information on required fields in it.<br/>
    Uncheck (not recommended), if you do not want this module to connect to vtiger now.'),
    '#weight' => 0,
  );
  $form['field_connection']['webform2vtiger_acckey'] = array(
    '#type' => 'textfield',
    '#title' => t('vtiger Unique Application Key'),
    '#weight' => -8,
    '#default_value' => variable_get('webform2vtiger_acckey', ''),
    '#description' => t("Application key is found in variable application_unique_key in vtiger configuration file [vtiger base]/config.inc.php vtiger user access key, available in 'My Preference' page of vtiger after you log in with user that is used to submit data. The same user and key should be put in vtiger Webforms configuration file [vtiger base]/modules/Webforms/Webforms.config.php"),
    '#required' => TRUE,
  );
  $form['field_connection']['webform2vtiger_username'] = array(
    '#type' => 'textfield',
    '#title' => t('vtiger username'),
    '#weight' => -7,
    '#default_value' => variable_get('webform2vtiger_username', ''),
    '#description' => t('Please type in vtiger username that the module will use to get information about available fields.'),
    '#required' => TRUE,
  );
  $form['field_connection']['webform2vtiger_user_accessk'] = array(
    '#type' => 'textfield',
    '#title' => t('vtiger user Access Key'),
    '#weight' => -6,
    '#default_value' => variable_get('webform2vtiger_user_accessk', ''),
    '#description' => t('This access key is found in "My Preferences" when you log in with the above typed in user.'),
    '#required' => TRUE,
  );

  $form['field_required'] = array(
    '#type' => 'fieldset',
    '#title' => t('vtiger CRM required fields'),
    '#description' => '<p>' . t('Please check vtiger lead fields that are required, ie they have to be mapped.') . '</p>',
  );

  $options = variable_get('webform2vtiger_available_fields', array());
  foreach ($options as $option_name => $option_value) {
    if ($option_name != check_plain($option_name)) {
      unset($options[$option_name]);
      $option_name = check_plain($option_name);
    }
    $options[$option_name] = check_plain($option_value);
  }

  $form['field_required']['webform2vtiger_required_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Required fields of vtiger CRM lead'),
    '#options' => $options,
    '#default_value' => variable_get('webform2vtiger_required_fields', array('lastname' => 'lastname')),
    '#description' => t('Required vtiger CRM lead fields will be marked automatically and customs fields added automatically if you allow this module to connect to your vtiger CRM installation (check above). Otherwise they could be determined by manually creating in vtiger CRM new lead and looking which fields have red * next to them.'),
    '#weight' => 0,
  );
  unset($form['field_required']['webform2vtiger_required_fields']['#options']['none']);

  $form = system_settings_form($form);
  $form['#submit'][] = 'webform2vtiger_admin_settings_submit_callback';

  return $form;
}


/**
 * After saving settings giving more hints how to proceed.
 *
 * @see webform2vtiger_admin_settings()
 */
function webform2vtiger_admin_settings_submit_callback($form, &$form_state) {
  drupal_set_message(t("Now you can edit webforms' fieldsets - webforms are available under !link Edit or create webform, then click Edit and select tab Webform.", array(
    '!link' => l(t('Administer > Content management > Webforms'), 'admin/content/webform'),
  )));
}


/**
 * Validating vtiger url, fetching lead fields and syncing them with
 * this module.
 *
 * @see webform2vtiger_admin_settings()
 */
function webform2vtiger_vtigercrm_url_validate($element, &$form_state) {
  if (!valid_url($element['#value'], TRUE)) {
    form_error($element, t('URL %url is invalid. Please enter a fully-qualified URL, such as http://www.example.com/crm', array('%url' => $element['#value'])));
  }

  // Check that url has no trailing "/".
  elseif ($element['#value'][strlen($element['#value']) - 1] == '/') {
    form_error($element, t('Please remove trailing "/" from the provided URL %url', array('%url' => $element['#value'])));
  }

  // Check if Drupal needs to try url by issuing HTTP requests.
  elseif ($form_state['values']['webform2vtiger_try_url']) {
    // Check that Drupal can issue HTTP requests.
    if (variable_get('drupal_http_request_fails', TRUE) && !system_check_http_request()) {
      drupal_set_message(t('Your system or network configuration does not allow Drupal to access web pages, resulting in inability to use webform2vtiger module. This could be due to your webserver configuration or PHP settings, and should be resolved in order to download information about available updates, fetch aggregator feeds, sign in via OpenID, or use other network-dependent services.'), 'error');
    }

    // Check that vtiger url is working.
    elseif (!webform2vtiger_check_site_works($element['#value'])) {
      form_error($element, t('Provided vtiger URL (%url) is not working - no site at this address.', array(
        '%url' => $element['#value'],
      )));
    }

    // Check that vtiger Webform module url is working.
    elseif (!webform2vtiger_check_site_works($element['#value'] . '/modules/Webforms/post.php')) {
      form_error($element, t('It looks like there is no Webforms plugin installed on vtiger CRM, since the address %url is not working. Please make sure vtiger has installed Webforms plugin before continuing with webform2vtiger module.', array(
        '%url' => $element['#value'] . '/modules/Webforms/post.php',
      )));
    }

    // Get leads fields names and their type from vtiger.
    else {
      $webform2vtiger_remote_fields = webform2vtiger_get_remote_fields($element['#value'] . '/webservice.php',
        $form_state['values']['webform2vtiger_username'], $form_state['values']['webform2vtiger_user_accessk']);
      if ($webform2vtiger_remote_fields) {
        variable_set('webform2vtiger_remote_fields', $webform2vtiger_remote_fields);
        foreach ($webform2vtiger_remote_fields as $id => $lead_field_array) {
          $field_value = !empty($form_state['values']['webform2vtiger_required_fields'][$lead_field_array['name']]) ? $form_state['values']['webform2vtiger_required_fields'][$lead_field_array['name']] : NULL;
          if ($lead_field_array['mandatory'] && $field_value == '0') {
            // Checking the required field on the form.
            $form_state['values']['webform2vtiger_required_fields'][$lead_field_array['name']] = $lead_field_array['name'];
            drupal_set_message(t('Found that your vtiger CRM lead form is requiring this field and therefore checked it as required: %required', array('%required' => $lead_field_array['label'])), 'warning');
          }
        }
      }

      // Now checking if vtiger Unique Application Key is correct.
      $data = 'appKey=' . $form_state['values']['webform2vtiger_acckey'];
      $result = drupal_http_request($element['#value'] . '/modules/Webforms/post.php',
        array(
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9'),
          'POST',
          $data
          );
      if (preg_match('/WEBFORMS_INVALID_APPKEY/', $result->data)) {
        drupal_set_message(t('Your provided vtiger Unique Application Key is incorrect! Application key is found in variable application_unique_key in vtiger configuration file [vtiger base]/config.inc.php'), 'error');
      }
    }
  }
}


/**
 * Getting lead fields from vtiger CRM remote site via vtiger webservices.
 *
 * @param string $webservice_url
 *   The url of vtiger webservices.
 * @param string $username
 *   The vtiger username.
 * @param string $user_accessk
 *   The vtiger user access key.
 *
 * @return array|bool
 *   Array of vtiger lead fields on success, FALSE on failure to get these
 *   fields.
 *
 * @see webform2vtiger_vtigercrm_url_validate()
 */
function webform2vtiger_get_remote_fields($webservice_url, $username, $user_accessk) {
  if (user_access('configure vtiger CRM connection')) {
    // Get a challenge token from the server. This must be a GET request.
    $result = drupal_http_request("$webservice_url?operation=getchallenge&username=" . urlencode($username));
    if (isset($result->data)) {
      $response_array = json_decode($result->data, TRUE);
      if (!$response_array['success']) {
        drupal_set_message(t('Could not get token from vtiger.'), 'error');
      }
      else {
        $token = $response_array['result']['token'];
        $post_data = '';
        $post_data .= '&accessKey=' . md5($token . $user_accessk);
        $post_data .= '&operation=login';
        $post_data .= '&username=' . urlencode($username);
        $post_data = ltrim($post_data, '&');
        // Login to the server using the challenge token obtained in get
        // challenge operation.
        $result2 = drupal_http_request("$webservice_url", array('Content-Type' => 'application/x-www-form-urlencoded'), 'POST', $post_data);
        $response_array2 = json_decode($result2->data, TRUE);
        if (!$response_array2['success']) {
          $error = $response_array2['error'];
          drupal_set_message(t('Could not login to vtiger - invalid "vtiger user Access Key".'), 'error');
        }
        else {
          $session_id = $response_array2['result']['sessionName'];
          $user_id = $response_array2['result']['userId'];
          $webservices_api_version = $response_array2['result']['version'];
          $vtiger_version = $response_array2['result']['vtigerVersion'];

          // Get the type information about a given Vtiger object.
          $result = drupal_http_request("$webservice_url?operation=describe&sessionName=$session_id&elementType=Leads");
          $response_array = json_decode($result->data, TRUE);
          if (!$response_array['success']) {
            $error = $response_array['error'];
            drupal_set_message(t('Could not complete vtiger operation - error: %error_code: %error_message.', array('%error_code' => $error['code'], '%error_message' => $error['message'])), 'error');
          }
          else {
            $vtiger_lead_fields = $response_array['result']['fields'];
            drupal_set_message(t('Connected to vtiger CRM successfully! Your vtiger CRM version is: %vtiger_version, webservices API version is: %webservice_api_version.', array('%vtiger_version' => $vtiger_version, '%webservice_api_version' => $webservices_api_version)));
            // Reporting to screen on found mandatory and custom lead fields.
            $mandatory_fields = '';
            $saved_required_fields = variable_get('webform2vtiger_required_fields', array());

            $custom_fields = '';
            // Recreating all available fields.
            $saved_available_fields_old = variable_get('webform2vtiger_available_fields', array());
            variable_del('webform2vtiger_available_fields');
            $saved_available_fields = array();
            foreach ($vtiger_lead_fields as $id => $lead_field_array) {
              // Collecting mandatory fields.
              // Field 'assigned_user_id' is taken care by vtiger Webform
              // plugin, therefore we do not require it.
              if ($lead_field_array['mandatory'] && $lead_field_array['name'] != 'assigned_user_id') {
                $mandatory_fields .= ', ' . $lead_field_array['name'];
              }

              // Collecting available fields, that were not saved yet.
              if (!isset($saved_available_fields[$lead_field_array['name']])) {
                $custom_fields .= ', ' . $lead_field_array['name'];
                $saved_available_fields[$lead_field_array['name']] = $lead_field_array['label'] . ' (' . $lead_field_array['type']['name'] . ')';
              }
            }
            variable_set('webform2vtiger_available_fields', $saved_available_fields);
            $mandatory_fields = ltrim($mandatory_fields, ', ');
            drupal_set_message(t('You vtiger CRM leads have these mandatory fields: %fields.', array('%fields' => $mandatory_fields)));

            $custom_fields = ltrim($custom_fields, ', ');
            if ($custom_fields != '') {
              variable_set('webform2vtiger_available_fields', $saved_available_fields);
              drupal_set_message(t('We found in you vtiger CRM leads these fields and saved them: %fields.', array('%fields' => $custom_fields)));
            }

            // Collecting for removal locally stored vtiger fields,
            // that are not in remote vtiger CRM.
            $removing_fields = '';
            foreach ($saved_available_fields_old as $fieldname => $fieldlabel) {
              $is_in_vtiger = FALSE;
              foreach ($vtiger_lead_fields as $id => $lead_field_array) {
                if ($fieldname == $lead_field_array['name'] || $fieldname == 'none') {
                  $is_in_vtiger = TRUE;
                  break;
                }
              }
              if (!$is_in_vtiger) {
                $removing_fields .= ', ' . $fieldname;
                // Removing existing mapping of this field to webform,
                // if there is any.
                db_query("DELETE FROM {webform2vtiger} WHERE tfield = '%s'", $fieldname);
              }
            }
            $removing_fields = ltrim($removing_fields, ', ');
            if ($removing_fields != '') {
              drupal_set_message(t('Removing saved vtiger fields that are not in remote vtiger CRM: %fields.', array('%fields' => $removing_fields)));
            }

            // Logout from vtiger CRM.
            $result = drupal_http_request("$webservice_url?operation=logout&sessionName=$session_id");
            return $vtiger_lead_fields;
          }
        }
      }
    }
  }
  return FALSE;
}


/**
 * Checking if remote site is working.
 *
 * @param string $url
 *   The url of remote site.
 *
 * @return bool
 *   TRUE if remote site returns result code between 100 and 399, FALSE on
 *   other return code or failure to connect.
 *
 * @see webform2vtiger_vtigercrm_url_validate()
 */
function webform2vtiger_check_site_works($url) {
  // Try to get the content of the url via drupal_http_request().
  $result = drupal_http_request($url);
  $works = isset($result->code) && ($result->code >= 100) && ($result->code < 400);
  return $works;
}
